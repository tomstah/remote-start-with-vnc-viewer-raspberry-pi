Simple programm to start computer via JFP on mainboard, if wake over lan is not available.
1) Connect power pin from mainboard to pin 11 and ground pin on rpi
2) make sure realvnc is installed and works properly
3) make sure python 3 as well as RPi.GPIO is installed
4) hit 'python /PATHOFFILE/start.py' while remote controll the pi from anywhere